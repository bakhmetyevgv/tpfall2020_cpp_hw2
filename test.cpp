#include <gtest/gtest.h>
#include <random>
#include <cstddef>
#include <dlfcn.h>
#define FILE_SIZE 100*1024*1024

extern "C" {
    #include "sequential.h"
    #include "input_file.h"
}


TEST(ParallelTest_med, start) {
    void *library;    
    library = dlopen("libparallel.so", RTLD_LAZY);
    if(!library){
        printf("PANIC\n");
    }
    void *gptr = dlsym(library, "find_substrings") ;
    //pure pain...
    int* (*find_substrings_par)(char *, size_t , char *[], size_t ) = 
        reinterpret_cast<int* (*)(char *, size_t , char *[], size_t )>(reinterpret_cast<long>(gptr));
    
    if(find_substrings_par == NULL){
        printf("PANIC2\n");
    }

    char file_name[] = "data_med.txt";

    int num_of_substrings = 3;
    char **substrings = (char **)malloc(sizeof(char *) * num_of_substrings);
    
    substrings[0] = (char *)malloc(strlen("alpha") * sizeof(char));
    strcpy(substrings[0], "alpha");

    substrings[1] = (char *)malloc(strlen("bravo") * sizeof(char));
    strcpy(substrings[1], "bravo");

    substrings[2] = (char *)malloc(strlen("foxtrot") * sizeof(char));
    strcpy(substrings[2], "foxtrot");

    char *data = (char *)calloc(FILE_SIZE, sizeof(char));

    FILE *f = fopen(file_name, "rb");
    if (f == NULL){
        fprintf(stderr, "Failed to open file\n");
    }
    int size = input_from_file(f, data);  
    fclose(f);

    int *res = (*find_substrings_par)(data, size, substrings, num_of_substrings);
    dlclose(library);

    EXPECT_EQ(res[0], 1006);
    EXPECT_EQ(res[1], 2010);
    EXPECT_EQ(res[2], 666);

    free(res);
    free(data);
    for(int i = 0; i < num_of_substrings; i++){
        free(substrings[i]);
    }
    free(substrings);
}


// TEST(ParallelTest_big, start) {
//     void *library;    
//     library = dlopen("libparallel.so", RTLD_LAZY);
//     if(!library){
//         printf("PANIC\n");
//     }
//     void *gptr = dlsym(library, "find_substrings") ;
//     //pure pain...
//     int* (*find_substrings_par)(char *, size_t , char *[], size_t ) = 
//         reinterpret_cast<int* (*)(char *, size_t , char *[], size_t )>(reinterpret_cast<long>(gptr));
    
//     if(find_substrings_par == NULL){
//         printf("PANIC2\n");
//     }

//     char file_name[] = "data.txt";

//     int num_of_substrings = 4;
//     char **substrings = (char **)malloc(sizeof(char *) * num_of_substrings);
    
//     substrings[0] = (char *)malloc(strlen("apple") * sizeof(char));
//     strcpy(substrings[0], "apple");

//     substrings[1] = (char *)malloc(strlen("acid") * sizeof(char));
//     strcpy(substrings[1], "acid");

//     substrings[2] = (char *)malloc(strlen("cherry") * sizeof(char));
//     strcpy(substrings[2], "cherry");

//     substrings[3] = (char *)malloc(strlen("nirvana") * sizeof(char));
//     strcpy(substrings[3], "nirvana");

//     char *data = (char *)calloc(FILE_SIZE, sizeof(char));

//     FILE *f = fopen(file_name, "rb");
//     if (f == NULL){
//         fprintf(stderr, "Failed to open file\n");
//     }
//     int size = input_from_file(f, data);  
//     fclose(f);

//     int *res = (*find_substrings_par)(data, size, substrings, num_of_substrings);
//     dlclose(library);
    
//     EXPECT_EQ(res[0], 1000);
//     EXPECT_EQ(res[1], 10021);
//     EXPECT_EQ(res[2], 22024);
//     EXPECT_EQ(res[3], 44539);

//     free(res);
//     free(data);
//     for(int i = 0; i < num_of_substrings; i++){
//         free(substrings[i]);
//     }
//     free(substrings);

// }

TEST(ComparisonTest, start) {
    void *library;    
    library = dlopen("libparallel.so", RTLD_LAZY);
    if(!library){
        printf("PANIC\n");
    }
    void *gptr = dlsym(library, "find_substrings") ;
    //pure pain...
    int* (*find_substrings_par)(char *, size_t , char *[], size_t ) = 
        reinterpret_cast<int* (*)(char *, size_t , char *[], size_t )>(reinterpret_cast<long>(gptr));
    
    if(find_substrings_par == NULL){
        printf("PANIC2\n");
    }

    char file_name[] = "data_med.txt";

     int num_of_substrings = 3;
    char **substrings = (char **)malloc(sizeof(char *) * num_of_substrings);
    
    substrings[0] = (char *)malloc(strlen("alpha") * sizeof(char));
    strcpy(substrings[0], "alpha");

    substrings[1] = (char *)malloc(strlen("bravo") * sizeof(char));
    strcpy(substrings[1], "bravo");

    substrings[2] = (char *)malloc(strlen("foxtrot") * sizeof(char));
    strcpy(substrings[2], "foxtrot");

    char *data = (char *)calloc(FILE_SIZE, sizeof(char));

    FILE *f = fopen(file_name, "rb");
    if (f == NULL){
        fprintf(stderr, "Failed to open file\n");
    }
    int size = input_from_file(f, data);  
    fclose(f);

    int *res_parallel = (*find_substrings_par)(data, size, substrings, num_of_substrings);
    dlclose(library);

    int *res_sequential = find_substrings(data, size, substrings, num_of_substrings);


    EXPECT_EQ(res_parallel[0], res_sequential[0]);
    EXPECT_EQ(res_parallel[1], res_sequential[1]);
    EXPECT_EQ(res_parallel[2], res_sequential[2]);

    free(res_parallel);
    free(res_sequential);
    free(data);
    for(int i = 0; i < num_of_substrings; i++){
        free(substrings[i]);
    }
    free(substrings);

}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}