#ifndef TPFALL2020_CPP_HW2_PARALLEL_H
#define TPFALL2020_CPP_HW2_PARALLEL_H

#define M_Type 1

typedef struct msgbuf {
    long mType;
    int s;
    int i;
} msgbuf;

struct partition {
    long mType;
    size_t start;
    size_t end;
};

int *find_substrings(char *data, size_t data_size, char *substrings[], size_t num_of_substrings);

void MakePartition(size_t start, size_t end, int partition_queue_qid);

#endif //TPFALL2020_CPP_HW2_PARALLEL_H
