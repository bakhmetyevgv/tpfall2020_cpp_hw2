#ifndef TPFALL2020_CPP_HW2_SEQUENTIAL_H
#define TPFALL2020_CPP_HW2_SEQUENTIAL_H

int *find_substrings(char *data, size_t data_size, char *substrings[], size_t num_of_substrings);

#endif //TPFALL2020_CPP_HW2_SEQUENTIAL_H