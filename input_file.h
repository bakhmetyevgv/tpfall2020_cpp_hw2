#ifndef TPFALL2020_CPP_HW2_INPUT_FILE_H
#define TPFALL2020_CPP_HW2_INPUT_FILE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int input_from_file(FILE *f, char *data);

#endif