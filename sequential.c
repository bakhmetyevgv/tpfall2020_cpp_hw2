#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sequential.h"

#define MIN(a,b) (((a)<(b))?(a):(b))

int *find_substrings(char *data, size_t data_size, char **substrings, size_t num_of_substrings){
    int *indexes = (int *)calloc(num_of_substrings, sizeof(int));
    for(int i = 0; i < num_of_substrings; indexes[i++] = -1);

    for(int i = 0, j = 0; i < data_size; i++){
        for(int s = 0; s < num_of_substrings; s++){
            size_t len = strlen(substrings[s]);
            for(j = 0; j < len && j < data_size; j++){
                if(substrings[s][j] != data[i + j])
                    break;
            }
            if(j == strlen(substrings[s])){
                if(indexes[s] == -1)
                    indexes[s] = i;
                else
                    indexes[s] = MIN(indexes[s], i);   
            }
        }
    }

    return indexes;
}