#include "input_file.h"


int input_from_file(FILE *f, char *data){
    int size = 0;
    while (!feof(f)){
        fscanf(f, "%c", &data[size++]);
    }     

    return size;
}
