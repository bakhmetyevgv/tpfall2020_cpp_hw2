#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <dlfcn.h>
#include "sequential.h"
#include "input_file.h"

#define FILE_SIZE 100 * 1024 * 1024

int main(int argc, char *argv[]){
    int opt, n;
    char *opts = ":f:", flag;
    void *library;
    int* (*find_substrings_par)(char *data, size_t data_size, char *substrings[], size_t num_of_substrings);

    while(getopt(argc, argv, opts) != -1){
        switch (optopt){
        case 'p':
            flag = optopt;
            break;
        case 's':
            flag = optopt;
            break;
        case ':':
            fprintf(stderr, "missing %c\n", optopt);
            break;
        default:
            break;
        }
    }

    switch (flag){
    case 'p':
        library = dlopen("libparallel.so", RTLD_LAZY);
        if(!library){
            printf("PANIC\n");
        }
        find_substrings_par = dlsym(library, "find_substrings");
        if(find_substrings_par == NULL){
            printf("PANIC2\n");
        }
        break;
    case 's': find_substrings_par = NULL; break;
    default: break;
    }
    

    char *file_name = argv[optind];

    char *data = (char *)calloc(FILE_SIZE, sizeof(char));
    if (data == NULL){
        fprintf(stderr, "Failed to allocate memory\n");
        return -1;
    }

    FILE *f = fopen(file_name, "rb");
    if (f == NULL){
        fprintf(stderr, "Failed to open file\n");
        return -1;
    }

    int size = input_from_file(f, data);  
    fclose(f);

    int num_of_substrings = argc - 3;

    char **substrings = (char **)malloc(sizeof(char *) * num_of_substrings);
    
    for(int i = 0; i < num_of_substrings; i++){
        substrings[i] = (char *)malloc(strlen(argv[optind + 1 + i]) * sizeof(char));
        strcpy(substrings[i], argv[optind + 1 + i]);
    }

    //Замер времени работы
    clock_t t = clock();
    double tt;
    
    int *res;
    if(flag == 'p'){
        res = (*find_substrings_par)(data, size, substrings, num_of_substrings);
        tt = (double) (clock() - t) / CLOCKS_PER_SEC;
        dlclose(library);
    }

    else{
        res = find_substrings(data, size, substrings, num_of_substrings);
    }
    
    tt = (double) (clock() - t) / CLOCKS_PER_SEC;

    printf("Program took %f second(s) to complete\n", tt);

    for(int i = 0; i < num_of_substrings; i++){
        printf("index of substring \"%s\" is %d\n", substrings[i] , res[i]);
    }   

    for(int i = 0; i < num_of_substrings; i++){
        free(substrings[i]);
    }

    free(res);
    free(data);
    free(substrings);

    return 0;
}