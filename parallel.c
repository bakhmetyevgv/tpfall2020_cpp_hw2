#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include "parallel.h"
#include "limits.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define PROCESSES_NUM sysconf(_SC_NPROCESSORS_ONLN)

void MakePartition(size_t start, size_t end, int partition_queue_qid) {
    struct partition part;
    part.mType = M_Type;
    part.start = start;
    part.end = end;
    msgsnd(partition_queue_qid, &part, sizeof(struct partition) - sizeof(long), 0);

}

int *find_substrings(char *data, size_t data_size, char *substrings[], size_t num_of_substrings){
    int msg_qid, status, partition_queue_qid;

    int *indexes = (int *)calloc(num_of_substrings, sizeof(int));
    for(int i = 0; i < num_of_substrings; indexes[i++] = -1);

    if ((msg_qid = msgget(IPC_PRIVATE, IPC_CREAT | 0660)) == -1) {
        fprintf(stderr, "Msg queue creation error");
    }

    if ((partition_queue_qid = msgget(IPC_PRIVATE, IPC_CREAT | 0660)) == -1) {
        fprintf(stderr, "Partition queue creation error");
    }

    int length_msg = sizeof(msgbuf) - sizeof(long);
    int length_part = sizeof(struct partition) - sizeof(long);


    int chunk_size = data_size / PROCESSES_NUM;
    int last_chunk_size = data_size - chunk_size * (PROCESSES_NUM - 1); 

    for (int i = 1; i < PROCESSES_NUM; ++i) {
        MakePartition( (i - 1) * chunk_size, i * chunk_size - 1, partition_queue_qid);
    }
    MakePartition(data_size - last_chunk_size, data_size - 1, partition_queue_qid);

    pid_t *pid_array = (pid_t *) malloc(sizeof(pid_t) * PROCESSES_NUM);

    pid_t main_pid = getpid();

    for (int k = 0; k < PROCESSES_NUM - 1; k++) {
        if (getpid() == main_pid) {
            pid_array[k] = fork();
        }
    }
    

    struct partition partition;
    msgrcv(partition_queue_qid, &partition, length_part, M_Type, 0);

    int index_local[num_of_substrings];
    for(int i = 0; i < num_of_substrings; index_local[i++] = data_size + 1);

    for(size_t i = partition.start, j = 0;i <= partition.end && i < data_size; ++i){
        for(int s = 0; s < num_of_substrings; ++s){
            size_t len = strlen(substrings[s]);
            
            for(j = 0; j < len && i + j < data_size; ++j){
                if(substrings[s][j] != data[i + j]){
                    break;
                }
                    
            }
            if(j == strlen(substrings[s])){;
                index_local[s] = i;
            }
        }
    }

    if (getpid() != main_pid) {
        for(int i = 0; i < num_of_substrings; ++i){
            msgbuf buf;
            buf.mType = 1;
            buf.s = i;
            buf.i = index_local[i];
            
            msgsnd(msg_qid, &buf, length_msg, 0);
        }
        exit(0);
    }


    for (int i = 0; i < PROCESSES_NUM - 1; ++i) {
        waitpid(pid_array[i], &status, 0);
    }

    for(int i = 0; i < (PROCESSES_NUM - 1) * num_of_substrings; ++i){
        msgbuf receive;
        msgrcv(msg_qid, &receive, length_msg, M_Type, 0);

        if(receive.i != data_size + 1){
            if(indexes[receive.s] == -1)
                indexes[receive.s] = receive.i;
            else
                indexes[receive.s] = MIN(indexes[receive.s], receive.i);
        }
             
    }

    free(pid_array);
    return indexes;
}
