#include <stdio.h>
#include <errno.h>

int main() {
    FILE *f = fopen("data_med.txt", "w");
    if(f == NULL){
        printf("Error %d \n", errno);
        return -1;
    }

    for(int i = 0; i < 10000; i++){
	    if(i == 1000){
            fprintf(f, "%s", "alpha");
            continue;
        }
        if(i == 2000){
            fprintf(f, "%s", "bravo");
            continue;
        }
        if(i == 3000){
            fprintf(f, "%s", "charlie");
            continue;
        }
        if(i == 4000){
            fprintf(f, "%s", "delta");
            continue;
        }
        if(i == 5000){
            fprintf(f, "%s", "echo");
            continue;
        }
        if(i == 666){
            fprintf(f, "%s", "foxtrot");
            continue;
        }

        if(i == 7000){
            fprintf(f, "%s", "golf");
            continue;
        }
        if(i == 8000){
            fprintf(f, "%s", "hotel");
            continue;
        }
        if(i == 666){
            fprintf(f, "%s", "india");
            continue;
        }

        if(i == 8563){
            fprintf(f, "%s", "alpha");
            continue;
        }

        if(i == 9453){
            fprintf(f, "%s", "bravo");
            continue;
        }

        fprintf(f, "%c", '-');    
    }

    fclose(f);

    return 0;
}


// int main() {
//     FILE *f = fopen("data.txt", "w");
//     if(f == NULL){
//         printf("Error %d \n", errno);
//         return -1;
//     }

//     for(int i = 0; i < 100 * 1024 * 1024 - (4 * 1024 * 1024); i++){
// 	    if(i == 1000){
//             fprintf(f, "%s", "apple");
//             continue;
//         }

//         if(i == 3500){
//             fprintf(f, "%s", "warrior");
//             continue;
//         }

//         if(i == 5000){
//             fprintf(f, "%s", "Donnie_Darko");
//             continue;
//         }

//         if(i == 10000){
//             fprintf(f, "%s", "acid");
//             continue;
//         }

//         if(i == 22000){
//             fprintf(f, "%s", "cherry");
//             continue;
//         }

//         if(i == 33000){
//             fprintf(f, "%s", "dragon");
//             continue;
//         }

//         if(i == 44000){
//             fprintf(f, "%s", "waggon");
//             continue;
//         }

//         if(i == 44500){
//             fprintf(f, "%s", "nirvana");
//             continue;
//         }

//         if(i == 45500){
//             fprintf(f, "%s", "acdc");
//             continue;
//         }

//         if(i == 46500){
//             fprintf(f, "%s", "kiss");
//             continue;
//         }

//         if(i == 47500){
//             fprintf(f, "%s", "metallica");
//             continue;
//         }

//         if(i == 48500){
//             fprintf(f, "%s", "scorpions");
//             continue;
//         }

//         if(i == 49500){
//             fprintf(f, "%s", "joy_division");
//             continue;
//         }

//         if(i == 60000){
//             fprintf(f, "%s", "motorama");
//             continue;
//         }

//         if(i == 70000){
//             fprintf(f, "%s", "whaeva");
//             continue;
//         }

//         if(i == 85000){
//             fprintf(f, "%s", "foreva");
//             continue;
//         }

//         if(i == 92000){
//             fprintf(f, "%s", "pretty");
//             continue;
//         }

//         if(i == 100000){
//             fprintf(f, "%s", "sssxxxsss");
//             continue;
//         }

//         if(i == 112000){
//             fprintf(f, "%s", "useful");
//             continue;
//         }

//         if(i == 125000){
//             fprintf(f, "%s", "awesome");
//             continue;
//         }

//         if(i == 135000){
//             fprintf(f, "%s", "nice");
//             continue;
//         }

//         if(i == 145000){
//             fprintf(f, "%s", "girl");
//             continue;
//         }

//         if(i == 155000){
//             fprintf(f, "%s", "boy");
//             continue;
//         }

//         if(i == 165000){
//             fprintf(f, "%s", "lmao");
//             continue;
//         }

//         if(i == 220000){
//             fprintf(f, "%s", "banana");
//             continue;
//         }

//         if(i == 104850){
//             fprintf(f, "%s", "wasd");
//             continue;
//         }

//         if(i == 1048500){
//             fprintf(f, "%s", "abcd");
//             continue;
//         }

//         if(i == 10485760){
//             fprintf(f, "%s", "qwerty");
//             continue;
//         }

//         fprintf(f, "%c", '-');    
//     }

//     fclose(f);

//     return 0;
// }
